import { RolesEnum } from '../enums/rolesEnum';

export interface User {
	id: string;
	firstName: string;
	lastName: string;
	email: string;
	password: string;
	salt: string;
	createdAt: Date;
	updatedAt: Date;
	appRole: RolesEnum;
	organisations: any;
	googleUser: any[];
	appleUser: any[];
	avatar: any;
}
