import { Action, createAction, createReducer, on, props } from '@ngrx/store';

export const getClients = createAction('Get clients', props<{ clients: any[] }>());
export const createClient = createAction('Create client');

export const initialState: any = [];

const clientsReducerObj = createReducer(
	initialState,
	on(getClients, (state, { clients }) => {
		console.log(clients);
		return [...clients];
	}),
	on(createClient, (state) => state),
);

export function clientsReducer(state: never[] | undefined, action: Action) {
	return clientsReducerObj(state, action);
}
