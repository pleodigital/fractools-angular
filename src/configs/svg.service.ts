import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
	providedIn: 'root',
})
export class SvgService {
	constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
		iconRegistry.addSvgIcon(
			'my-star-icon',
			sanitizer.bypassSecurityTrustResourceUrl('assets/icons/my-star-icon.svg'),
		);
	}
}
