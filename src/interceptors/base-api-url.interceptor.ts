import { Injectable } from '@angular/core';
import {
	HTTP_INTERCEPTORS,
	HttpEvent,
	HttpHandler,
	HttpHeaders,
	HttpInterceptor,
	HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (request.url.includes('i18n')) {
			return next.handle(request);
		}
		const headers = new HttpHeaders().set(
			'x-organisation-id',
			'e7ec1530-d8c7-4774-8d52-5f7019fc234d',
		);
		const apiReq = request.clone({
			url: `${environment.api + request.url}`,
			headers,
			withCredentials: true,
		});
		return next.handle(apiReq);
	}
}

export const baseUrlInterceptor = {
	provide: HTTP_INTERCEPTORS,
	useClass: BaseUrlInterceptor,
	multi: true,
};
