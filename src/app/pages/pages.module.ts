import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { SharedModule } from '../_shared/_shared.module';
import { PagesComponent } from './pages.component';
import { MenuComponent } from './menu/menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { menuTranslations } from '../main-translations-modules';
import { HttpClient } from '@angular/common/http';

@NgModule({
	declarations: [PagesComponent, MenuComponent, DashboardComponent],
	imports: [
		CommonModule,
		PagesRoutingModule,
		SharedModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: menuTranslations,
				deps: [HttpClient],
			},
			isolate: true,
		}),
	],
})
export class PagesModule {}
