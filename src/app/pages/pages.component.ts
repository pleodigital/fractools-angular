import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';

@Component({
	selector: 'app-pages',
	templateUrl: './pages.component.html',
	styleUrls: ['./pages.component.scss'],
})
export class PagesComponent implements OnInit {
	constructor(private translate: TranslateService) {
		translate.addLangs(['en', 'pl']);
		translate.setDefaultLang('en');
		registerLocaleData(localePl);
		const browserLang = translate.getBrowserLang();
		translate.use(browserLang.match(/en|pl/) ? browserLang : 'pl');
	}

	ngOnInit(): void {}
}
