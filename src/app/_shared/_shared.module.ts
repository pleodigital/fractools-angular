import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';

const materials = [MatIconModule];

@NgModule({
	declarations: [],
	imports: [CommonModule, materials, ReactiveFormsModule],
	exports: [ReactiveFormsModule],
})
export class SharedModule {}
