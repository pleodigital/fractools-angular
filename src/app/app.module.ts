import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { authTranslations } from './main-translations-modules';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './_auth/login/login.component';
import { SharedModule } from './_shared/_shared.module';
import { RegisterComponent } from './_auth/register/register.component';
import { ForgotPasswordComponent } from './_auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './_auth/reset-password/reset-password.component';
import { baseUrlInterceptor } from '../interceptors/base-api-url.interceptor';
import { clientsReducer } from '../reducers/clients.reducer';
import { tokenInterceptor } from './_auth/auth.interceptor';

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		RegisterComponent,
		ForgotPasswordComponent,
		ResetPasswordComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		SharedModule,
		HttpClientModule,
		BrowserAnimationsModule,
		StoreModule.forRoot({ clients: clientsReducer }),
		TranslateModule.forRoot({
			extend: true,
			loader: {
				provide: TranslateLoader,
				useFactory: authTranslations,
				deps: [HttpClient],
			},
			isolate: true,
		}),
	],
	providers: [tokenInterceptor, baseUrlInterceptor],
	bootstrap: [AppComponent],
})
export class AppModule {}
