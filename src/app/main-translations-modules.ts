import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';

export function authTranslations(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/auth/', '.json');
}

export function menuTranslations(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/menu/', '.json');
}
