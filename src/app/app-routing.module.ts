import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './_auth/login/login.component';
import { AuthGuard } from './_auth/auth.guard';
import { RegisterComponent } from './_auth/register/register.component';
import { ForgotPasswordComponent } from './_auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './_auth/reset-password/reset-password.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: '/app',
		pathMatch: 'full',
	},
	{
		path: 'app',
		loadChildren: () => import('src/app/pages/pages.module').then((m) => m.PagesModule),
		canActivate: [AuthGuard],
	},
	{
		path: 'login',
		component: LoginComponent,
	},
	{
		path: 'signup',
		component: RegisterComponent,
	},
	{
		path: 'forgot-password',
		component: ForgotPasswordComponent,
	},
	{
		path: 'reset-password',
		component: ResetPasswordComponent,
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
