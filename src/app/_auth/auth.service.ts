import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { LoginFormData } from '../../models/loginFormData';

@Injectable({
	providedIn: 'root',
})
export class AuthService {
	protected organizationId: string;

	constructor(private http: HttpClient, private router: Router) {}

	getOrganizationId() {
		return this.organizationId;
	}

	logIn(formData: LoginFormData) {
		return this.http.post<User>(`/auth/signin`, formData).subscribe(
			(response) => {
				this.organizationId = response.organisations[0]
					? response.organisations[0].id
					: null;
				localStorage.setItem('FractoolsUserData', JSON.stringify(response));
			},
			() => {},
			() => {
				return this.router.navigate(['/app']);
			},
		);
	}

	refreshToken() {
		return this.http.post<{ user: User; accessToken: string }>(`/auth/refresh`, {});
	}

	logOut() {
		localStorage.removeItem('FractoolsUserData');
		return this.router.navigate(['/login']);
	}
}
