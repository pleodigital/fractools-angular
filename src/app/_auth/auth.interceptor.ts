import { Injectable } from '@angular/core';
import {
	HTTP_INTERCEPTORS,
	HttpEvent,
	HttpHandler,
	HttpInterceptor,
	HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
	constructor(public authService: AuthService) {}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		request = this.addToken(request);

		return next.handle(request).pipe(
			catchError((error): Observable<any> => {
				if (error.status === 401 && !request.url.includes('login')) {
					return this.handle401Error(request, next);
				} else if (error.status === 422 && request.url.includes('refresh')) {
					this.authService.logOut();
					return new Observable<any>();
				} else {
					return throwError(error);
				}
			}),
		);
	}

	private addToken(request: HttpRequest<any>) {
		const organizationId = this.authService.getOrganizationId();
		if (request.url.includes('upload')) {
			return request.clone({
				withCredentials: true,
				headers: request.headers.set(
					'X-Organisation-Id',
					organizationId ? organizationId.trim() : '',
				),
			});
		} else {
			return request.clone({
				withCredentials: true,
				headers: request.headers
					.set('Content-Type', 'application/json')
					.set('X-Organisation-Id', organizationId ? organizationId.trim() : ''),
			});
		}
	}

	private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
		return this.authService.refreshToken().pipe(
			take(1),
			switchMap(() => next.handle(this.addToken(request))),
		);
	}
}

export const tokenInterceptor = {
	provide: HTTP_INTERCEPTORS,
	useClass: TokenInterceptor,
	multi: true,
};
