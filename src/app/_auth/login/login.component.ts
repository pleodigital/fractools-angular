import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;

	constructor(private formBuilder: FormBuilder, private authService: AuthService) {}

	ngOnInit() {
		this.buildForm();
	}

	buildForm() {
		this.loginForm = this.formBuilder.group({
			email: ['test@test.pl', Validators.required],
			password: ['12345', Validators.required],
		});
	}

	login() {
		this.authService.logIn(this.loginForm.getRawValue());
	}
}
