import { CanActivate, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { User } from '../../models/user';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
	constructor(private authService: AuthService, private router: Router) {}

	canActivate(): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
		const userData: User = JSON.parse(<string>localStorage.getItem('FractoolsUserData'));
		if (userData) {
			return true;
		}
		return this.router.createUrlTree(['/login']);
	}
}
