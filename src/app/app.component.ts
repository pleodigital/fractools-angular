import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	constructor(public translate: TranslateService) {
		translate.addLangs(['en', 'pl']);
		translate.setDefaultLang('en');
		registerLocaleData(localePl);
		const browserLang = translate.getBrowserLang();
		translate.use(browserLang.match(/en|pl/) ? browserLang : 'pl');
	}
}
