import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { getClients } from '../../reducers/clients.reducer';

@Injectable({
	providedIn: 'root',
})
export class ClientsService {
	constructor(private http: HttpClient, private store: Store<{ clients: any[] }>) {}

	getAllClients() {
		this.http
			.get<any[]>(`/organisation/clients/list`, { withCredentials: true })
			.subscribe((clients) => {
				this.store.dispatch(getClients({ clients }));
			});
	}

	createClient(body: any) {
		this.http.post(`/organisation/clients/add`, body).subscribe();
	}
}
